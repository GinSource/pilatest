/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author SergioEverardo
 */
public class LocalEventListener extends MouseAdapter implements WindowListener,ActionListener {

    JButton _boton;
    JTextArea _textArea;
    Pila _pila;

    public LocalEventListener(JButton boton, JTextArea caja, Pila pila) {
        _boton = boton;
        _boton.addActionListener(this);
        _textArea = caja;
        _pila = pila;
    }
    
    
    
    @Override
    public void windowOpened(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowIconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    
        // int clickCount = e.getClickCount();
        // JOptionPane.showMessageDialog(null, "hola" + clickCount);
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String nombreBoton = _boton.getText();
        String[] split = nombreBoton.split(":");
        String idStr = split[1].trim();
        
        int id = Integer.parseInt(idStr);
        
        Revista revista = _pila.BuscarRevista(id);
        Libro libro = _pila.BuscarLibro(id);
        Periodico periodico = _pila.BuscarPeriodico(id);
        String contenido = "";
        if(revista != null){
            contenido = "Nombre: " + revista.getNombre() + "\n Portada: " + revista.getArtisPortada() + " \n N° Paginas: " + revista.getNumPag() + " \n Fecha Publicación: " + revista.getFechaP();
        }
        if(libro != null){
            
        }
        
        if(periodico != null){
            
        }
        
        _textArea.setText(contenido);
    }
    
}
