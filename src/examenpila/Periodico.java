/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

/**
 *
 * @author José
 */
public class Periodico extends Publicacion{
    private String [] seciones;
    
    public Periodico(){
    
    }

    public Periodico(String[] seciones) {
        this.seciones = seciones;
    }

    public String[] getSeciones() {
        return seciones;
    }

    public void setSeciones(String[] seciones) {
        this.seciones = seciones;
    }
    
}
