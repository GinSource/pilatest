/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

import java.util.Date;

/**
 *
 * @author José
 */
public class Publicacion {
    private int numPag;
    private String nombre;
    private Date fechaP;
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Publicacion(int numPag, String nombre, Date fechaP, int id) {
        this.numPag = numPag;
        this.nombre = nombre;
        this.fechaP = fechaP;
        this.id = id;
    }
    public Publicacion(){
    
    }

    public int getNumPag() {
        return numPag;
    }

    public void setNumPag(int numPag) {
        this.numPag = numPag;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaP() {
        return fechaP;
    }

    public void setFechaP(Date fechaP) {
        this.fechaP = fechaP;
    }
    
}
