/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

/**
 *
 * @author José
 */
public class Libro extends Publicacion{
    private String autor;
    private String editoria;
    
    public Libro(){
    
    }
    public Libro(String autor, String editoria) {
        this.autor = autor;
        this.editoria = editoria;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditoria() {
        return editoria;
    }

    public void setEditoria(String editoria) {
        this.editoria = editoria;
    }
    
    
}
