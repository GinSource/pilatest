/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SergioEverardo
 */
public class Pila {
    List<Publicacion> _pila;
    List<Libro> _libros;
    List<Periodico> _periodicos;
    List<Revista> _revista;
    Integer idGenerico;

    
    public Pila(List<Publicacion> pilas, List<Libro> libros, List<Periodico> periodicos, List<Revista> revistas, Integer id){
        _pila = pilas;
        _libros = libros;
        _periodicos = periodicos;
        _revista = revistas;
        idGenerico = id;
    }
    
    
  
    public boolean AgregarLibro(Libro obj){
        obj.setId(GeneradorId());
        if(_pila.size() < 8){
            _libros.add(obj);
            _pila.add(obj);
            return false;
        }
        
        return true;
    }
    
    public Revista AgregarRevista(Revista obj){
        obj.setId(GeneradorId());
        if(_pila.size() < 8){
            
            _revista.add(obj);
            _pila.add(obj);
            return obj;
        }
        
        return null;
    }
    
    public boolean AgregarPeriodico(Periodico obj){
        obj.setId(GeneradorId());
        if(_pila.size() < 8){
            _periodicos.add(obj);
            _pila.add(obj);
            return false;
        }
        
        return true;
    }
    
    public Libro BuscarLibro(Integer id){
        
        for(Libro p : _libros)
        {
            if(p.getId() == id){
                return p;
            }
        }
        
        return null;
    }
    
    public Revista BuscarRevista(Integer id){
        
        for(Revista p : _revista)
        {
            if(p.getId() == id){
                return p;
            }
        }
        
        return null;
    }
    
    public Periodico BuscarPeriodico(Integer id){
        
        for(Periodico p : _periodicos)
        {
            if(p.getId() == id){
                return p;
            }
        }
        
        return null;
    }
    
    public Publicacion BuscarPublicacion(Integer id){
        
        for(Publicacion p : _pila)
        {
            if(p.getId() == id){
                return p;
            }
        }
        
        return new Publicacion();
    }
    
    public List<Publicacion> MostrarPila(){
        return _pila;
    }
    
    public void Borrar(int id){
        Revista revista = this.BuscarRevista(id);
        Libro libro = this.BuscarLibro(id);
        Periodico periodico = this.BuscarPeriodico(id);
        Publicacion publicacion = this.BuscarPublicacion(id);

        if(revista != null){
            _revista.remove(revista);
        }
        if(libro != null){
            _libros.remove(libro);
        }
        
        if(periodico != null){
            _periodicos.remove(periodico);
        }
        
        if(publicacion != null){
            _pila.remove(publicacion);
        }
    }
    
    private Integer GeneradorId(){
        return idGenerico++;
    }
    
}
