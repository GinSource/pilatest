/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.button.StandardButtonShaper;

/**
 *
 * @author José
 */
public class FramePilas extends javax.swing.JFrame {

    /**
     * Creates new form FramePilas
     */
    int cont = 0;
    int contadorClick = 1;
    int posBot = 0;
    int idObj = 0;
    JButton arr[] = new JButton[10];
    JButton arr2[] = new JButton[10];
    Pila _pila;
    LocalEventListener _listener;

    public FramePilas() {
        initComponents();
        diseños();
        arreglo();
        arreglo2();
        pnlLibro.setVisible(false);
        pnlRevista.setVisible(false);
        pnlPeriodico.setVisible(false);
        agrega();
        _pila = new Pila(new ArrayList<Publicacion>(), new ArrayList<Libro>(), new ArrayList<Periodico>(), new ArrayList<Revista>(), 0);
    }

    public void diseños() {
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.CremeSkin");
        SubstanceLookAndFeel.setCurrentTheme("org.jvnet.substance.theme.SubstanceLighAquaTheme");
        this.btnRedondo.putClientProperty(SubstanceLookAndFeel.BUTTON_SHAPER_PROPERTY,
                new StandardButtonShaper());
    }
    public void arreglo2(){
        pnlPilaBtns2.setVisible(false);
        pnlPilaBtns2.setLayout(new GridLayout(10,1) );
        for (int i = 0; i < 10; i++) {
            pnlPilaBtns2.add(arr2[i] = new JButton(" ")).setVisible(false);
        }
       pnlPilaBtns2.setVisible(true);
    }
    public void arreglo() {
        pnlPilaBtns.setVisible(false);
        pnlPilaBtns.setLayout(new GridLayout(10, 1));
        
        for (int i = 0; i < 10; i++) {
            pnlPilaBtns.add(arr[i] = new JButton(" ")).setVisible(false);
            
            arr[i].addActionListener(_listener);
        }
        pnlPilaBtns.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        grupoEyB = new javax.swing.ButtonGroup();
        rbtPeriodico = new javax.swing.JRadioButton();
        rbtLibro = new javax.swing.JRadioButton();
        rbtRevista = new javax.swing.JRadioButton();
        BtnAgregar = new javax.swing.JButton();
        pnlPilaBtns = new javax.swing.JPanel();
        pnlPilaBtns2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtArea = new javax.swing.JTextArea();
        btnRedondo = new javax.swing.JButton();
        pnlTodo = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        TxtNombre = new javax.swing.JTextField();
        calendar = new com.toedter.calendar.JCalendar();
        pnlLibro = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtAutor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtEditoria = new javax.swing.JTextField();
        pnlPeriodico = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtSecciones = new javax.swing.JTextField();
        pnlRevista = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txtPortada = new javax.swing.JTextField();
        spnPag = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        radioBuscar = new javax.swing.JRadioButton();
        radioEliminar = new javax.swing.JRadioButton();
        TextBYE = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        buttonGroup1.add(rbtPeriodico);
        rbtPeriodico.setText("Periodico");
        rbtPeriodico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtPeriodicoActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbtLibro);
        rbtLibro.setText("Libro");
        rbtLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtLibroActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbtRevista);
        rbtRevista.setText("Revista");
        rbtRevista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtRevistaActionPerformed(evt);
            }
        });

        BtnAgregar.setText("Agregar");
        BtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlPilaBtnsLayout = new javax.swing.GroupLayout(pnlPilaBtns);
        pnlPilaBtns.setLayout(pnlPilaBtnsLayout);
        pnlPilaBtnsLayout.setHorizontalGroup(
            pnlPilaBtnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        pnlPilaBtnsLayout.setVerticalGroup(
            pnlPilaBtnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 202, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pnlPilaBtns2Layout = new javax.swing.GroupLayout(pnlPilaBtns2);
        pnlPilaBtns2.setLayout(pnlPilaBtns2Layout);
        pnlPilaBtns2Layout.setHorizontalGroup(
            pnlPilaBtns2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        pnlPilaBtns2Layout.setVerticalGroup(
            pnlPilaBtns2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        txtArea.setColumns(20);
        txtArea.setRows(5);
        jScrollPane1.setViewportView(txtArea);

        btnRedondo.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnRedondo.setText("Redondo");
        btnRedondo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRedondoActionPerformed(evt);
            }
        });

        jLabel2.setText("Fecha Publicacion:");

        jLabel1.setText("Nombre:");

        jLabel3.setText("Autor: ");

        jLabel4.setText("Editoria:");

        javax.swing.GroupLayout pnlLibroLayout = new javax.swing.GroupLayout(pnlLibro);
        pnlLibro.setLayout(pnlLibroLayout);
        pnlLibroLayout.setHorizontalGroup(
            pnlLibroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLibroLayout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(txtAutor, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEditoria, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlLibroLayout.setVerticalGroup(
            pnlLibroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLibroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlLibroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtAutor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtEditoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel5.setText("Secciones:");

        javax.swing.GroupLayout pnlPeriodicoLayout = new javax.swing.GroupLayout(pnlPeriodico);
        pnlPeriodico.setLayout(pnlPeriodicoLayout);
        pnlPeriodicoLayout.setHorizontalGroup(
            pnlPeriodicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPeriodicoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSecciones, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );
        pnlPeriodicoLayout.setVerticalGroup(
            pnlPeriodicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPeriodicoLayout.createSequentialGroup()
                .addGroup(pnlPeriodicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtSecciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jLabel6.setText("ArtisPortada: ");

        javax.swing.GroupLayout pnlRevistaLayout = new javax.swing.GroupLayout(pnlRevista);
        pnlRevista.setLayout(pnlRevistaLayout);
        pnlRevistaLayout.setHorizontalGroup(
            pnlRevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRevistaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPortada, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );
        pnlRevistaLayout.setVerticalGroup(
            pnlRevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlRevistaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlRevistaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtPortada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel7.setText("NumPag:");

        javax.swing.GroupLayout pnlTodoLayout = new javax.swing.GroupLayout(pnlTodo);
        pnlTodo.setLayout(pnlTodoLayout);
        pnlTodoLayout.setHorizontalGroup(
            pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTodoLayout.createSequentialGroup()
                .addGroup(pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlTodoLayout.createSequentialGroup()
                        .addGroup(pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlTodoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(pnlPeriodico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlTodoLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(pnlRevista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlTodoLayout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(pnlLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(27, 27, 27))
                    .addGroup(pnlTodoLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(spnPag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)))
                .addComponent(calendar, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlTodoLayout.setVerticalGroup(
            pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTodoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlTodoLayout.createSequentialGroup()
                        .addGroup(pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlTodoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(TxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(spnPag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel7))
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlLibro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlPeriodico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(pnlRevista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlTodoLayout.createSequentialGroup()
                        .addComponent(calendar, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        grupoEyB.add(radioBuscar);
        radioBuscar.setText("Buscar");

        grupoEyB.add(radioEliminar);
        radioEliminar.setText("Eliminar");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radioBuscar)
                    .addComponent(radioEliminar))
                .addGap(0, 14, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(radioBuscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(radioEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(336, 336, 336)
                .addComponent(BtnAgregar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(rbtLibro)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rbtPeriodico)
                .addGap(201, 201, 201)
                .addComponent(rbtRevista)
                .addGap(168, 168, 168))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlTodo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlPilaBtns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pnlPilaBtns2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(TextBYE, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(50, 50, 50)))
                        .addComponent(btnRedondo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(139, 139, 139)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbtRevista)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbtLibro)
                        .addComponent(rbtPeriodico)))
                .addGap(18, 18, 18)
                .addComponent(BtnAgregar)
                .addGap(18, 18, 18)
                .addComponent(pnlTodo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnRedondo, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(TextBYE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnlPilaBtns, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlPilaBtns2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 32, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void agrega() {
        if (rbtLibro.isSelected()) {
            pnlLibro.setVisible(true);
            
        } else if (rbtPeriodico.isSelected()) {
            pnlPeriodico.setVisible(true);
            
        } else if (rbtRevista.isSelected()) {
            pnlRevista.setVisible(true);
        }
        
    }
    
    private void BtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAgregarActionPerformed
        int m=1;
        cont++;
        _listener = new LocalEventListener(arr[10 - cont], txtArea, _pila);
        if (rbtRevista.isSelected()) {
            Revista r = new Revista(txtPortada.getText(),(int)spnPag.getValue(),TxtNombre.getText(),calendar.getDate(), 0);
            Revista revista = _pila.AgregarRevista(r);
            arr[10 - cont].setText(revista.getNombre()+ " : " + revista.getId());
            arr[10 - cont].setVisible(true);

            
        } else if (rbtPeriodico.isSelected()) {
            
            arr[10 - cont].setText(TxtNombre.getText());
            arr[10 - cont].setVisible(true);
            
        } else {
            
            arr[10 - cont].setText(TxtNombre.getText());
            arr[10 - cont].setVisible(true);
        }
        
    }//GEN-LAST:event_BtnAgregarActionPerformed

    private void rbtLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtLibroActionPerformed
        if(rbtLibro.isSelected()){
        pnlLibro.setVisible(true);
        pnlRevista.setVisible(false);
        pnlPeriodico.setVisible(false);
        }
    }//GEN-LAST:event_rbtLibroActionPerformed

    private void rbtPeriodicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtPeriodicoActionPerformed
        if(rbtPeriodico.isSelected()){
        pnlLibro.setVisible(false);
        pnlRevista.setVisible(false);
        pnlPeriodico.setVisible(true);
        }
    }//GEN-LAST:event_rbtPeriodicoActionPerformed

    private void rbtRevistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtRevistaActionPerformed
        
        if(rbtRevista.isSelected()){
        pnlLibro.setVisible(false);
        pnlRevista.setVisible(true);
        pnlPeriodico.setVisible(false);
        }
    }//GEN-LAST:event_rbtRevistaActionPerformed

    private void btnRedondoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRedondoActionPerformed
        boolean eliminarOption = radioEliminar.isSelected();
        boolean buscarOption = radioBuscar.isSelected();
        String id = TextBYE.getText();
        if(buscarOption){
            contadorClick = 1;
            int idActual = Integer.parseInt(id.trim());
            Revista revista = _pila.BuscarRevista(idActual);
            Libro libro = _pila.BuscarLibro(idActual);
            Periodico periodico = _pila.BuscarPeriodico(idActual);
            String contenido = "";
            if(revista != null){
                contenido = "Nombre: " + revista.getNombre() + "\n Portada: " + revista.getArtisPortada() + " \n N° Paginas: " + revista.getNumPag() + " \n Fecha Publicación: " + revista.getFechaP();
            }
            if(libro != null){
                
            }

            if(periodico != null){
                
            }

            txtArea.setText(contenido);
        }
        
        if(eliminarOption){
            if(contadorClick == 1){
                boolean aux= false;
                int contAux = 9;
                for (int i = 9; i >= 0; i--){
                    String botontxt = arr[i].getText();
                    if(botontxt.indexOf(":") >= 0){
                        String[] split = botontxt.split(":");
                        String idActual = split[1].trim();
                        idObj = Integer.parseInt(id.trim());
                        if(idActual.equals(id.trim())){
                            aux = true;
                            posBot = i;
                            continue;
                        }

                        if(aux){
                            arr[i].setText("");
                            arr[i].setVisible(false);
                            arr2[contAux].setText(botontxt);
                            arr2[contAux].setVisible(true);
                            contAux--;
                        }
                    }
                }
            }
            
            if(contadorClick == 2){
                arr[posBot].setText("");
                arr[posBot].setVisible(false);
                _pila.Borrar(idObj);
            }
            
            if(contadorClick == 3)
            {
                int auxArr1 = posBot;
                for (int i = 9; i >= 0; i--){
                    String botontxt = arr2[i].getText();
                    if(botontxt.indexOf(":") >= 0){
                        String[] split = botontxt.split(":");
                        String idActual = split[1].trim();
                        arr[auxArr1].setText(botontxt);
                        arr[auxArr1].setVisible(true);
                        arr2[i].setText("");
                        arr2[i].setVisible(false);
                        auxArr1--;
                    }
                }
            }
            
            contadorClick++;
        }
        
        
    }//GEN-LAST:event_btnRedondoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FramePilas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FramePilas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FramePilas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FramePilas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FramePilas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAgregar;
    private javax.swing.JTextField TextBYE;
    private javax.swing.JTextField TxtNombre;
    private javax.swing.JButton btnRedondo;
    private javax.swing.ButtonGroup buttonGroup1;
    private com.toedter.calendar.JCalendar calendar;
    private javax.swing.ButtonGroup grupoEyB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlLibro;
    private javax.swing.JPanel pnlPeriodico;
    private javax.swing.JPanel pnlPilaBtns;
    private javax.swing.JPanel pnlPilaBtns2;
    private javax.swing.JPanel pnlRevista;
    private javax.swing.JPanel pnlTodo;
    private javax.swing.JRadioButton radioBuscar;
    private javax.swing.JRadioButton radioEliminar;
    private javax.swing.JRadioButton rbtLibro;
    private javax.swing.JRadioButton rbtPeriodico;
    private javax.swing.JRadioButton rbtRevista;
    private javax.swing.JSpinner spnPag;
    private javax.swing.JTextArea txtArea;
    private javax.swing.JTextField txtAutor;
    private javax.swing.JTextField txtEditoria;
    private javax.swing.JTextField txtPortada;
    private javax.swing.JTextField txtSecciones;
    // End of variables declaration//GEN-END:variables
}
