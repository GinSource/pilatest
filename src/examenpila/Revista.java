/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpila;

import java.util.Date;

/**
 *
 * @author José
 */
public class Revista extends Publicacion{
    private String artisPortada;

    public Revista() {
    }

    public Revista(String artisPortada, int numPag, String nombre, Date fechaP, int id) {
        super(numPag, nombre, fechaP, id);
        this.artisPortada = artisPortada;
    }

    public String getArtisPortada() {
        return artisPortada;
    }

    public void setArtisPortada(String artisPortada) {
        this.artisPortada = artisPortada;
    }
    
    
}
